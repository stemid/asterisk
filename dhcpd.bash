#!/usr/bin/env bash

set -x

nic="${1:-enp4s0f3u2}"

touch dhcpd/dhcpd.leases
sudo dhcpd -d -pf dhcpd/dhcpd.pid \
  -cf dhcpd/dhcpd.conf \
  -lf dhcpd/dhcpd.leases "$nic"
