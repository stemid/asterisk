# Asterisk container for DECT phones with PostgreSQL DB

* Work in progress
* Depends on podman and buildah

## Build and start containers

This starts;

* a postgresql detached
* an asterisk container in the foreground

    bash Containerfile.bash

## Run dhcp server

    bash dhcpd.bash

# Configuration

More to come...

# RFP

## Factory Reset

Format a USB stick with FAT and place a single file called **factoryreset** on it, power up the RFP and wait. If the file has been removed from the USB stick then the RFP has been reset.

When doing factory reset the first LED should glow solid orange, and after reset is complete LEDs 1-3 should blink orange.
If first LED (from the bottom) glows solid red then the firmware is likely corrupt.

# See also

* [https://howto.dect.network/#powering-the-rfp](howto.dect.network docs)
