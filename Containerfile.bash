#!/usr/bin/env bash

set -euo pipefail
set -x

if [ "${1:-''}" = 'clean' ]; then
  podman rm -if dnsmasq asterisk postgresql
  podman network rm -f dect-dnsmasq
  exit 0
fi

dnsmasq_image="${DNSMASQ_IMAGE:-localhost/dnsmasq:latest}"
asterisk_image="${ASTERISK_IMAGE:-localhost/asterisk:latest}"
postgres_image="${POSTGRES_IMAGE:-docker.io/postgres:14}"
entrypoint="${ENTRYPOINT:-/docker-entrypoint.sh}"

# Build containers

ctr="$(buildah from docker.io/alpine:latest)"

buildah add --chmod 0755 "$ctr" docker-entrypoint.sh /docker-entrypoint.sh

buildah config --entrypoint "$entrypoint" "$ctr"
buildah config --port 5060/tcp "$ctr"
buildah config --port 5060/udp "$ctr"
buildah config --env LC_ALL=C.UTF-8 "$ctr"
buildah config --env LANG=C.UTF-8 "$ctr"

buildah run "$ctr" apk add --update --quiet asterisk \
  man-pages psqlodbc bash

buildah commit "$ctr" "$asterisk_image"

echo "$asterisk_image: $ctr"

# 2nd container for dnsmasq
ctr="$(buildah from docker.io/alpine:latest)"

buildah run "$ctr" apk add --update --quiet dnsmasq \
  man-pages bash
buildah config --entrypoint /usr/sbin/dnsmasq "$ctr"
buildah commit "$ctr" "$dnsmasq_image"

# Run containers

podman run --name "dnsmasq" -d --rm --network host \
  -v "$(pwd)/dnsmasq/dnsmasq.conf:/etc/dnsmasq.conf:Z" \
  -v "$(pwd)/dnsmasq/dnsmasq.leases:/var/lib/dnsmasq/dnsmasq.leases:Z" \
  "$dnsmasq_image"

mkdir -p postgresql/{pgdata,pgsocket}

if ! podman container exists postgresql; then
  podman run --name "postgresql" -d --rm \
    -v "$(pwd)/postgresql/pgdata:/var/lib/postgresql/data:Z" \
    -v "$(pwd)/postgresql/pgsocket:/var/run/postgresql:Z" \
    -v "$(pwd)/postgresql/init.sql:/docker-entrypoint-initdb.d/init.sql:ro" \
    -e "POSTGRES_DB=${POSTGRES_DB:-postgres}" \
    -e "POSTGRES_USER=${POSTGRES_USER:-postgres}" \
    -e "POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-postgres}" \
    -p "5432:5432" \
    "$postgres_image"
fi

mkdir -p log

podman run --name "asterisk" --rm -it \
  -v "$(pwd)/log:/var/log/asterisk:Z" \
  -v "$(pwd)/config/odbc.ini:/etc/odbc.ini:Z" \
  -v "$(pwd)/config/odbcinst.ini:/etc/odbcinst.ini:Z" \
  -v "$(pwd)/config/asterisk.conf:/etc/asterisk/asterisk.conf:Z" \
  -v "$(pwd)/config/modules.conf:/etc/asterisk/modules.conf:Z" \
  -v "$(pwd)/config/sip.conf:/etc/asterisk/sip.conf:Z" \
  -v "$(pwd)/config/res_odbc.conf:/etc/asterisk/res_odbc.conf:Z" \
  "$asterisk_image"

