#!/usr/bin/env bash

set -e
apk add --update --quiet \
  asterisk \
  asterisk-sample-config \
  asterisk-pgsql \
  asterisk -U asterisk
sleep 5s

asterisk -rx "core show channeltypes" | grep -v PJSIP || \
  rm -rf /usr/lib/asterisk/modules/*pj*

pkill -9 aster
sleep 1s
truncate -s 0 \
  /var/log/asterisk/messages \
  /var/log/asterisk/queue_log

mkdir -p /var/spool/asterisk/fax
chown -R asterisk: /var/spool/asterisk
rm -rf /var/run/asterisk/* \
  /var/cache/apk/* \
  /tmp/* \
  /var/tmp/*
