#!/usr/bin/env bash

set -euo pipefail
set -x

sudo dnsmasq --log-debug -dC dnsmasq/dnsmasq.conf
